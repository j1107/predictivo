{
	"ErrorDatos" : false,
	"MensajeEstatus" : null,
	"ModEstatus" : null,
	"SeccionUsuario" : {
		"EmpleadoLlavePrimaria" : {
			"EmpleadoLlavePrimaria" : 129,
			"EmpleadoTipo" : {
				"EmpleadoTipo" : 0,
				"DescripcionTipoEmpleado" : ""
			},
			"EmpleadoNombre" : "",
			"EmpleadoUsr" : "",
			"EmpleadoStatus" : "",
			"EmpleadoNumero" : 0,
			"EmpleadoChk" : 0,
			"EmpleadoChkVisible" : "Collapsed"
		},
		"SeccionFIA" : 0,
		"ExtensionResponsable" : 0,
		"ResponsableSeccion" : "",
		"Cade" : "",
		"ColorSeccion" : "",
		"EstatusSeccion" : "",
		"NombreSeccion" : "PROGRAMAS",
		"CveSeccion" : 8,
		"MediaGridActual" : null,
		"MediaGridRuta" : null,
		"MediaGridTotal" : null
	},
	"ModSecciones" : {
		"AZT_SECCION" : "125",
		"ListaSecciones" : [{
				"EmpleadoLlavePrimaria" : {
					"EmpleadoLlavePrimaria" : 129,
					"EmpleadoTipo" : {
						"EmpleadoTipo" : 2,
						"DescripcionTipoEmpleado" : "FREELANCE"
					},
					"EmpleadoNombre" : "ENRIQUE VALDEZ BREMONT",
					"EmpleadoUsr" : "evaldes",
					"EmpleadoStatus" : "A",
					"EmpleadoNumero" : 60001761,
					"EmpleadoChk" : 0,
					"EmpleadoChkVisible" : "Collapsed"
				},
				"SeccionFIA" : 1,
				"ExtensionResponsable" : 0,
				"ResponsableSeccion" : null,
				"Cade" : "S",
				"ColorSeccion" : "#00FF00",
				"EstatusSeccion" : "A",
				"NombreSeccion" : "PROGRAMAS",
				"CveSeccion" : 8,
				"MediaGridActual" : "0",
				"MediaGridRuta" : null,
				"MediaGridTotal" : "0"
			},{
				"EmpleadoLlavePrimaria" : {
					"EmpleadoLlavePrimaria" : 129,
					"EmpleadoTipo" : {
						"EmpleadoTipo" : 2,
						"DescripcionTipoEmpleado" : "FREELANCE"
					},
					"EmpleadoNombre" : "ENRIQUE VALDEZ BREMONT",
					"EmpleadoUsr" : "evaldes",
					"EmpleadoStatus" : "A",
					"EmpleadoNumero" : 60001761,
					"EmpleadoChk" : 0,
					"EmpleadoChkVisible" : "Collapsed"
				},
				"SeccionFIA" : 1,
				"ExtensionResponsable" : 0,
				"ResponsableSeccion" : null,
				"Cade" : "S",
				"ColorSeccion" : "#00FF00",
				"EstatusSeccion" : "A",
				"NombreSeccion" : "PROGRAMAS",
				"CveSeccion" : 8,
				"MediaGridActual" : "0",
				"MediaGridRuta" : null,
				"MediaGridTotal" : "0"
			}
		]
	},
	"ModTipoNotaSeccion" : {
		"ListaTipoNota" : [{
				"TinoLlPr" : 3,
				"TnoDesc" : "BREVES",
				"SeccLlPr" : 8,
				"SeccDesc" : "PROGRAMAS",
				"TinoAbre" : "BREV"
			}, {
				"TinoLlPr" : 2,
				"TnoDesc" : "IMPORTANTES",
				"SeccLlPr" : 8,
				"SeccDesc" : "PROGRAMAS",
				"TinoAbre" : "IMPO"
			}, {
				"TinoLlPr" : 4,
				"TnoDesc" : "INVESTIGACIONES ESPECIALES ",
				"SeccLlPr" : 8,
				"SeccDesc" : "PROGRAMAS",
				"TinoAbre" : "IESP"
			}, {
				"TinoLlPr" : 66,
				"TnoDesc" : "OT DE PROGRAMA",
				"SeccLlPr" : 8,
				"SeccDesc" : "PROGRAMAS",
				"TinoAbre" : "OTPR"
			}, {
				"TinoLlPr" : 1,
				"TnoDesc" : "PRINCIPAL",
				"SeccLlPr" : 8,
				"SeccDesc" : "PROGRAMAS",
				"TinoAbre" : "PRIN"
			}
		],
		"AZT_TIPONOTA" : "1026"
	},
	"ModReporteros" : {
		"ListaReporteros" : [{
				"EmpleadoLlavePrimaria" : 2332,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : " NO REQUIERE REPORTERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200220,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6695,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : " NO REQUIERE REPORTERO CORAZON GRUPERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200222,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6685,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : " NO REQUIERE REPORTERO GAC",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200221,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2472,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ABISAI RUBIO(NVO LAREDO TAMPS CORRESPONSAL)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200454,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 168,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ADRIAN VIRGEN MIRANDA(CAMPECHE, CAMP.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002365,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2310,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Agencia Internacional de Noticias",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200185,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2311,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Agencia Nacional de Noticias",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200186,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6395,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALANY ADRIANA GOMEZ HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60048885,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 441,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALEJANDRO MEDINA ZAVALA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006271,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1678,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALEJANDRO VILLALVAZO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023051,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1823,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALFONSO HIDALGO MENDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024961,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5760,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALICIA GUTIERREZ OCHOA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60040340,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2473,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Alonso de la Colina Sordo",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200455,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1679,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALVARO LOPEZ SORDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023053,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2856,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Alvaro Ortiz",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000105,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 166,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "AMADA EVELYN CASTANON LEAL",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002332,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 372,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Ana Maria Lomeli Robles",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005567,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 760,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Anareli Nayatzi Palomares Martínez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011558,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 7097,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANDRES MANUEL LADRON DE GUEVARA SOLER",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60061911,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 63,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANDRES MARTINEZ FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60000756,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6285,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANGEL EDUARDO FRANCO ALVAREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60047223,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5653,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANTONIO CASTANEDA SANTOS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60034244,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2627,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANTONIO MORENO (COAHUILA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60222222,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 214,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANTONIO VERONICO ROSIQUE CEDILLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60003242,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 766,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ARACELI GARZA SAUCEDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011631,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1568,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ARIEL LEGUIZAMON CONTRERAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021578,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2269,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ARMANDO ALVARES LUNAR  (PUEBLA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200143,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2559,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ARMANDO GUZMAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200541,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5350,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ARMANDO PEREZ RUIZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60031944,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2045,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "AYLEN DEL TORO GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030224,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1275,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Beatriz González",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018010,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 838,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "BERENICE ANGELICA GARCIA HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012397,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2848,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS A. CAMPOS MAGALLANES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000005,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 991,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS ADALBERTO SOTO RAMIREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60014061,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 828,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Carlos Eliseo Torres Lara",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012299,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 617,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS LUIS GUERRERO GALLEGOS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60008717,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 378,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CAROLINA GALLO ESPINOZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005607,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1472,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CAROLINA MARIA ROCHA MENOCAL",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60020191,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6665,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CESAR CABALLERO RIVERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60055716,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2020,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CESAR CASTRO RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029285,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 927,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Cesar Méndez Ramírez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60013249,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6914,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CESAR OCTAVIO CRUZ RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60058827,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1716,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CHRISTIAN AMIN LEAL LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023291,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 574,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CHRISTIAN OCTAVIO MARTINOLI CURI",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007959,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 981,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Christian Paola Lara Hernandez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60013986,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1351,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CLAUDIA IVETT GARCIA FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018756,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2121,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CORRESPONSALES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60100187,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2961,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CRISTIAN ESTRADA (COAHUILA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 80000025,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4792,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Damaris Esdrey Ramirez Hernandez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030483,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2137,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Daniel de Rosas (Ecatepec, Edomex.Corresponsal)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200010,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2857,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANIEL GIRONES (BARCELONA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000106,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2496,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANIEL OSORIO(PUEBLA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200478,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2994,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Daniel Rojas (La Paz, BCS)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000058,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2011,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANIELA CELY LLAMAS RAMIREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029052,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2225,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "David Armenta Ramirez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200099,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3824,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DAVID ORTIZ MARQUET",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60014328,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6911,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DEBORAH CASSANDRA SUAREZ CHACON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60054488,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6773,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DIANA PATRICIA BALBUENA ESPINOSA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60056956,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1631,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DIEGO BORBOLLA ESPANA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022561,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4594,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DULCE ANGELICA CALZADA GARCIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019107,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 7038,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDEN HERNANDEZ LEYTE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 107184,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5724,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDER GENARO CALDERON JUAREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60034527,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2149,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDGAR ESTIG AVALOS ZUNIGA    (TAMPICO, TAMPS.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60010939,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 343,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Edgar Galicia",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005212,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2521,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Edrick García Guerrero",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200503,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 7014,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ELENA ORTIZ LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60063315,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1916,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ELIA GUTIERREZ SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60027388,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1690,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ELIAS LOZADA MARTINEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023153,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6782,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EMILIO ESPINOSA LARA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60057612,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1727,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENGELS ARTURO DOMINGUEZ ANGEL",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023462,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2947,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENRIQUE ARCADIO VIVEROS FELIX",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900015,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5234,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENRIQUE PARDO GENIS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032810,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1697,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENRIQUE ZAMORA IBARRA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023223,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 993,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ERIKA GROTHE HERRERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60014084,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 717,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ERY ROSEMBERG ACUNA MENESES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011050,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1882,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FABIOLA IVONNE ROJAS CORTES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026867,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 255,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FEDERICO ANAYA DURAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60003966,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1502,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FEDERICO HUGO VELA NAHUM",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60020613,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2978,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Fely Carnalla (Cuernavaca, Mor.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000042,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2996,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Fernando Arriaga (Zihuatanejo, Gro.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000060,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2255,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FERNANDO ESCOBEDO LARA (MONTERREY)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200129,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1112,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FERNANDO RENE HERNANDEZ LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016930,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 892,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GABRIEL DOMINGUEZ LOZADA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012884,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 257,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GABRIEL HERNANDEZ MIRANDA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60003975,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2628,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GAMALIEL LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60232323,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1201,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GEORGINA IVETTE HERNANDEZ FUENTES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017675,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4958,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GERARDO ALBERTO VARGAS TORRES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032068,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1332,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GERARDO ALONSO MELIN SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018459,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 60,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Gerardo Segura",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60000729,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 420,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GERMAN WING VARELA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006029,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2993,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Germán Zepeda (Toluca, Edomex.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000057,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6783,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GIBRAN KAZEN ANDRADE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60043583,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2880,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GLENDA ISIS GALARZA GUERRERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64003684,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 186,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Gloria Perez Jacome",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002666,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2953,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GONZALO CORDOVA (HERMOSILLO, SON)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900022,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1768,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GRETA QUETZALLI ROJAS SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024117,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2910,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GUADALUPE PATRICIA MUÑOZ GARCIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64004200,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6663,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GUILLERMO JAVIER GARDUÑO FALCON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60054316,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5495,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GUILLERMO SANTISTEBAN DE LA GARZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033820,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 201,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Hannia Novell",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60003124,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6942,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "HECTOR CASTILLO FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60054862,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2487,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "HÉCTOR MORENO(MÉRIDA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200469,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6858,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ILSE LORENA TREJO RUELAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60062640,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 751,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "INÉS SAINZ GALLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011458,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1348,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IRMA SELENE FLORES SOLANO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018737,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2044,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IRVING ROJANO PINEDA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030034,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1541,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ISAAC RAZIEL CRUZ SALAZAR",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021419,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6741,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ISIDRO CORRO RAMOS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60055403,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2980,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Israel Ruiz (Cuernavaca, Mor.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000044,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1529,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ITALIA DEL CARMEN SOLER GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021369,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 931,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ITZEL GARCIA BRIONES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60013279,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2983,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Itzel García (Querétaro)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000047,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2951,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IVAN OJEDA (SAN LUIS POTOSÍ, SLP)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900020,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5902,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IVAN SANTILLAN LOEZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60034067,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 104,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IYARI GONZALEZ TELLEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001671,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 105,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Jaime Guerrero",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001675,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 15,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JAVIER ALATORRE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60000130,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5865,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JAVIER GONZALEZ DAVILA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60034768,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2853,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JAVIER MARTINEZ BROCAL (ROMA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000100,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5621,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JAVIER MUJICA GOMEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033996,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6541,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JAVIER RAFAEL ROJAS SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60048802,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 444,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JESUS CISNEROS ORTIZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006277,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1843,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JESUS EDUARDO RUIZ REBOLLEDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026064,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1806,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JESUS JOEL FUENTES SANDOVAL",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024664,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6564,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JESUS RUBEN RAMOS DUARTE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60043521,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2428,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JONATHAN LARA (GUATEMALA, GUATEMALA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200410,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 147,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE ALFONSO ZARZA PINEDA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002046,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6232,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE CISNEROS MORALES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60046933,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2148,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE EDUARDO MUNOZ AVILES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011041,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 715,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Jorge Muñoz Aviles",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011041,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2984,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Jorge Oropeza (Oaxaca)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000048,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 728,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE PATINO CALDERON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011145,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2024,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE PINTO TAMAYO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029317,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 753,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE LUIS ALEJANDRO HERNANDEZ  RAMIREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026671,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1961,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE LUIS SALDANA CAMACHO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60028135,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2986,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "José Martín Sámano (Cancún, Q. Roo)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000050,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1641,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE RAUL REYES SANTIAGO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022649,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1728,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE ULISES CARRANZA GASTELUM",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023463,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 134,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN AGUSTIN BERNABE RODRIGUEZ CESPEDES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001864,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 107,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN ANTONIO HERNANDEZ BRETON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001677,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6317,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN ANTONIO MORENO MARTINEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60047940,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2141,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN BAUTISTA MUNOS CARRILLO (MAZATLAN SIN)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200014,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 94,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN CARLOS BARAJAS CORONADO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001614,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1966,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN CARLOS G CANTON LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60028157,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2215,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Juan Carlos Hernandez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200089,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6682,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN CARLOS PEREZ NAVARRO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60053936,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2009,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN FRANCISCO GONZALEZ GARCIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60028993,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1744,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN FRANCISCO ROCHA GUTIERREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023751,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2987,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Juan Guízar (Tijuana, BC.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000051,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2518,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN JOSE ROMERO RAMIREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200500,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6082,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN MANUEL PALOMAR MONTOYA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029365,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 578,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN PABLO SOLIS MORENO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007969,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6752,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JULIAN MOLINA RIVERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004102,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1764,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JULIO OMAR TELLEZ BUENDIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024070,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1408,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Karen Quintero",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019371,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2051,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "KARLA XOCHITL BALTAZAR GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030309,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5579,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "KATIA ISLA SOLTHER",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033817,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1108,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Laura Casillas",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016891,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6968,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LAURA PELAEZ GUZMAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60063833,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2145,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LEONARDO HERRERA (PACHUCA, HGO)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200018,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2245,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LETICIA BENAVIDES   (MONTERREY)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200119,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2988,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Leticia Pacheco (Coatzacoalcos, Ver.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000052,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 7016,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LILIANA MAYO PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60056379,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1197,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LINET PUENTE CASTRO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017669,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3561,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LIZBETH ZENDEJAS GUADARRAMA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022337,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6742,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUCERO ISABEL CRISTINA RODRIGUEZ TAMEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60055973,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 890,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUCIA SANDOVAL AMARO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012875,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1694,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ENRIQUE ALFONZO MUNOZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023190,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6427,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS REY PICAZO CAMACHO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60045967,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 812,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUZ IRENE CARMONA CONSTANTINO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012188,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 319,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO AURELIO MORALES FERRERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004878,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2479,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO GONZÁLEZ(CIUDAD VALLES)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200461,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6354,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARIA ANDREINA ANDRADE BACCA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60048446,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 275,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARIA DEL ROCIO MARTINEZ DEL VALLE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004281,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2151,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARICRUZ RIVERA GARZON  (TOLUCA, EDOMEX)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60010938,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2982,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Mario Bucio (Cuernavaca, Mor.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000046,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 386,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Maxi Pelaez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005691,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2068,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL SUSANO GILES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030805,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6611,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL URIARTE QUEZADA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60053941,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5980,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIRELLE ROMINA BARRERA FOURNIER",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60043459,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 460,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MONICA CASTANEDA RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006400,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 306,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MONICA MARIA GARZA GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004681,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 146,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Nacho Nuñez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002043,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5243,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "NAYELLI MATE GONZALEZ MENENDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032896,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1982,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "NINIVE LUANDA GONZALEZ GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60028474,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 470,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "NOEL PARIS DANIEL SANGEADO FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006502,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1460,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "NORMA YOLANDA MARQUEZ AVILA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60020071,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5883,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OMAR ALEJANDRO SAMANO GARCIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030682,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2025,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OMAR EUNAM VILLARREAL VILLALBAZO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029423,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5761,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OMAR SOTO VELASCO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030890,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1369,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSCAR GOMEZ ROMERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018950,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2840,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSCAR VERGARA (OAXACA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60666666,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5208,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OTONIEL MARTINEZ VARELA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032817,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6938,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PABLO CESAR SALAZAR AVILA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60062212,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5607,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PABLO DE RUBENS PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60034087,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2309,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PABLO MONZALVO (MILAN, ITALIA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200184,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2860,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PALOMA GOMEZ BORRERO ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62001405,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2150,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PEDRO GERARDO LOPEZ  (TAPACHULA, CHIS)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012800,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 887,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAFAEL AYALA LEDESMA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012853,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2284,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAMON ARTURO HERNANDEZ SALAZAR (VERACRUZ)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200158,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 651,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAMON FREGOSO PALAZON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60009306,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2865,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RANDY SUASTEGUI (GUERRERO)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 63333333,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1739,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAUL PATINO OLIVARES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023669,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6367,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAYMUNDO MICHEL SANTIAGO DIAZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60048674,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2964,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "REINA FRESCO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 82222222,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4701,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RENE ANTONIO MUNOZ LOZADA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022652,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2858,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "REYNA FRESCO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000107,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2424,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "REYNALDO ARAGÓN (LIMA, PERU)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200406,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2105,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "REYNALDO LARA RUIZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022475,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 294,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO ARMANDO OSORNIO DE LA PENA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004508,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6234,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO BARRERA LEÓN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033385,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2500,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO PÉREZ(TORREÓN)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200482,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5905,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO PRATZ CARRANZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032560,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2981,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Ricardo Torres (Cuernvaca, Mor.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000045,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1630,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO TORRES RIVERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022552,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1262,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Roberto Carlos Dominguez Fuentes",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017926,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6629,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ROCIO OJEDA TORRES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60047312,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2990,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Rocío Vivas (Campeche)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000054,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1423,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RODRIGO ALVAREZ CHAVEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019630,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1611,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ROSA MARIA BAUTISTA RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022278,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2249,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ROSA MARIA PEREDA RANGEL   (MONTERREY)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021371,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 254,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RUBEN HERNANDEZ MEDINA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60003958,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1103,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RUBEN OMAR MENDOZA HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016850,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1276,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RUTH ODETTE MELGAR DEL PINO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018011,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1100,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SALVADOR ABEL MACEDA MUNOZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016846,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1354,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Sandra Daniela Alcocer Núñez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018811,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 856,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SERGIO JOAQUIN SEPULVEDA DIAZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012519,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6627,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SILVIA CARMEN OTERO LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60054457,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6965,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SOFIA ROSA BERNAL ARAUJO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60056047,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2991,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Sonia Valenzuela (Zacatecas)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000055,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2859,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "STAN BIOKSIC (TORONTO)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000108,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2029,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "TANIA VENTIMILLA CONTRERAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029562,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 555,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ULISES VALENTE GRAJALES VALDIVIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007695,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1377,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "VERONICA SELENE CARRERA PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019030,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 504,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Vicente Galvez",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006917,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2855,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "VIVIANA AVILA LINDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 62000104,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2995,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Yahir Licona (Puebla)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000059,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1397,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ZEIZA GABRIELA PEREZ MURILLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019251,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}
		],
		"AZT_EQUIPOCOBERTURA" : "1020"
	},
	"ModCamarografos" : {
		"ListaCamarografos" : [{
				"EmpleadoLlavePrimaria" : 2329,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : " NO REQUIERE CAMAROGRAFO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200216,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2512,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALBERTO OLAN(VILLAHERMOSA, TAMPS.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200494,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5455,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALEJANDRO ALBERTO NEGRETE OJEDA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024453,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2934,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALEJANDRO ESPINOZA (ACAPULCO, GRO.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900000,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6109,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALFREDO BELTRAN VIANA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60013248,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 266,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALFREDO RAMIREZ SOTELO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004122,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 63,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANDRES MARTINEZ FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60000756,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 268,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANGEL CORTES DOMINGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004145,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3253,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANGEL GUILLERMO AVALOS ROMERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 410037,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 68,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "BENJAMIN ALVAREZ GARCIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60000948,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5461,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "BRUCE DANIEL MENDOZA MAYO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033478,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2948,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS CASTAÑEDA (MAZATLÁN, SIN.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900017,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2935,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS DIAZ (AGUASCALIENTES, AGS.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900001,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6578,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS ENRIQUE RANGEL OLVERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60045942,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1045,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS GOMEZ AGUILAR",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016400,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 277,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CIRIO GUTIERREZ HERNANDEZ Baja Folio 93181",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004289,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6478,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DAMIAN RAFAEL MARBAN REYES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60027827,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2939,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANTE ALFARO (COLIMA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900006,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2966,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DAVID IZUNZA (SALTILLO, COAH.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000030,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2942,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DIEGO CADENAS (CHIHUAHUA, CHIH.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900010,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4973,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDGAR TREJO MENDOZA ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024020,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3013,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENERGIA DEPORTIVA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999996,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2947,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENRIQUE ARCADIO VIVEROS FELIX",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900015,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4964,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ERMIDIO SERRANO LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60031992,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3012,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ESPECIALES 1 - MOTOS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999995,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3011,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ESPECIALES 2",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999994,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1147,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EVERALDO HERNANDEZ FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017350,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 298,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FERNANDO ALBARRAN RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004514,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6374,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FERNANDO GUERRERO ROMERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60048778,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2297,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FERNANDO TADASI NAKASIMA CANTU  (TORREON, COAHUILA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200171,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2537,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FRANCISCO MEDRANO HERAS (TOLUCA, EDOMEX)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200519,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3742,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "FRANCISCO NARANJO HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60020288,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 338,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GABRIEL CAMPOS MORENO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005162,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2938,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GABRIEL DITRICH (CD. JUÁREZ, CHIH.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900004,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 548,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GABRIEL PALMER ARIAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007645,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2973,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GILDARDO VAZQUEZ SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000037,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 640,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GUILLERMO CHAVARRIA ROMERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60009089,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3252,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GUSTAVO BELTRAN MEZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029862,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3010,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "HANDYCAM",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999993,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 935,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "HECTOR GALVAN BASURTO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60013291,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2536,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ILIANA CASTRELLÓN ANDAZOLA (CIUDAD JUÁREZ, CHIH.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200518,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1073,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ISRAEL BASURTO MADRID",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016642,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2946,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IVÁN ORTEGA (HERMOSILLO, SON.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900014,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2969,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JAVIER MARTÍNEZ (TEPIC, NAY.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000033,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2305,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JESUS ARMANDO SALAS RODRIGUEZ   (MAZATLAN, SINALOA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200179,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1564,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE AGUILAR CORTES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021516,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1014,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE ALBERTO GOMEZ GUARDIOLA ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60014423,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2936,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE CASANOVA (CAMPECHE, CAMP.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900002,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1365,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE FLORES SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018925,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1523,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE IBARRA GARCIA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021334,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2510,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE TORRES (TAMPICO, TAMPS.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200492,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 422,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE ALFONSO ARMENTA LLAMAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006057,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2505,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "José Luis López(Cuernavaca, Mor.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200487,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2506,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSÉ LUIS RUIZ (ECATEPEC, EDOMEX)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200488,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 792,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE LUIS SEGURA HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011945,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2937,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE LUIS UZCANGA GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900003,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6825,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE MARIA CERVERA NORIEGA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60061211,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3467,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE MARIA JIMENEZ VARELA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023474,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 69,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE MARTIN VALENCIA RUIZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001001,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6949,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE SAMUEL ELIZONDO ORTIZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60061506,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3740,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN JOSE VAZQUEZ OROZCO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60020351,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1176,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JULIO CESAR GRAW PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017527,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 299,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JULIO CESAR ORDONEZ CLEMENTE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004515,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1216,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ANTONIO GAMBOA LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017746,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 234,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ENRIQUE OLIVERA MARTINEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60003531,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 479,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ERNESTO ARTEAGA MONROY",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006623,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2968,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MANUEL DE JESÚS NÚÑEZ (TAPACHULA, CHIS.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000032,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 416,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO ANTONIO GOMEZ ISLAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006007,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 465,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO ANTONIO PEREZ ZUÑIGA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60006463,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6640,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO ANTONIO PINEDA VAZQUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004766,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1413,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Mario Alberto Medina Villeda",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019435,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 304,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARIO MENDOZA ASCENCIO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004644,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5458,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARTIN ANTONIO SALGADO GONZALEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012934,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 383,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MAURICIO GONZALEZ SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005633,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 714,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL BERNAL AVALOS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011037,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6305,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL MORALES PALMAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60045949,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 152,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL MORENO BARAJAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002125,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 297,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL PONCE SANCHEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004512,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2068,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL SUSANO GILES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030805,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 821,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ORLANDO BARRANCO MENDOZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012242,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2387,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSCAR LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200284,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5457,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSCAR MENDOZA ALVAREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60025995,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2700,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSCAR SANCHEZ PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 403335,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2273,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSWALDO OLMOS MORA (PUEBLA)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200147,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6468,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PABLO SEVILLA BOLAÑOS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60049675,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2100,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PAVEL IVAN COBOS LOO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60070000,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1575,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PEDRO ENRIQUE CORTES LICEA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021627,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2971,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAFAEL REYES (TOLUCA, EDOMEX)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000035,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2943,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAUL CRUZ (DURANGO, DGO.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900011,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3009,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "REDACCION",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999992,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1542,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO RUIZ JAUREGUI",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021425,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 296,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ROBERTO ANDRADE RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60004511,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 175,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ROBERTO CARLOS LOPEZ MARQUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002422,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2940,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ROBERTO FERIAS RODRIGUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 64900007,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2080,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RUBEN MONTIEL MENDOZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60031103,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2975,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SADDID MORA (VILLAHERMOSA, TAB.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 109000039,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5206,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SALVADOR CRUZ PAREDES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032058,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 560,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SALVADOR GARCIA JUAREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007803,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 384,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SALVADOR ZAMUDIO CABRERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005636,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 549,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SAUL DIAZ VAZQUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007646,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3008,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 1",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999991,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3007,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 2",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999990,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3006,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 3",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999989,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3005,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 4",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999988,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3004,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 5",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999987,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3003,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 6",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999986,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3002,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 7",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999985,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 3001,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SECCION DEPORTIVA 8",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999984,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1563,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SERGIO ADAN JIMENEZ MAY",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021513,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 642,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SERGIO ALDO LOPEZ BELTRAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60009120,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2508,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Sergio Díaz (Nvo. Laredo, Tamps.)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200490,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 336,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ULISES JARAMILLO GUZMAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60005120,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2511,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "VIRGILIO ALCOCER(CHETUMAL, Q. ROO)",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60200493,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}
		],
		"AZT_EQUIPOCOBERTURA" : "1020"
	},
	"ModEditores" : {
		"ListaEditores" : [{
				"EmpleadoLlavePrimaria" : 3016,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : " NO REQUIERE EDITOR DE CAMPO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 159999999,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2017,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANA PORTILLA ACEVEDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029250,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 6444,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ANGEL RENE NAVARRETE MERINO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60049514,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1466,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS JAVIER GUEVARA VELAZQUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60020157,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 114,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CLAUDIA PATRICIA MERA OVANDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60001710,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1165,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANIEL FLORES DURAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017434,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4885,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DAVID CORREA PIÑA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60016544,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1360,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Diego Najera De La Fuente",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018857,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1853,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDER JOSE ROMERO MARTIN DEL CAMPO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026108,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4864,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDGAR ALVAREZ CASTILLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60031252,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5490,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EDUARDO ABEL TORRES CARRILLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033344,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1333,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EILEEN NISHINO AIRIN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018464,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5692,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ELBA GONZALEZ MOTA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033878,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1712,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GERARDO FRANCISCO CRUCES ALVAREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023287,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2035,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GERARDO LOPEZ AGUILAR",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029689,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1875,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GLORIA IVONNE CEBALLOS LANDA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026698,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1125,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "HUGO ENRIQUE ANDONEGUI OLVERA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017116,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1869,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "HUGO RODOLFO FEREGRINO GUTIERREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026576,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5201,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IRVING ALFREDO RODRIGUEZ RAMIREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60031520,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 778,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "IVAN ESTEBAN UGALDE VAZQUEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011805,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2714,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE ANGEL GALLEGOS HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023259,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1844,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE JAVIER CRISOSTOMO LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026065,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1884,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE MANUEL CRUZ MENDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026875,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1703,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Liliana Tello Cuautli",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023261,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5489,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ALBERTO MARTINEZ GRESS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60033322,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 813,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ANGEL GONZALEZ LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012194,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1887,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ESTEBAN ARZATE PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026989,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 187,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS GUTIERREZ MURILLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60002681,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2703,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO ALFREDO TORRES HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023267,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1701,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCO ANTONIO CADENA MONTERDE",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023255,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1413,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "Mario Alberto Medina Villeda",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019435,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5223,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ORLANDO MONTES MENDOZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032857,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2700,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "OSCAR SANCHEZ PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 403335,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1575,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PEDRO ENRIQUE CORTES LICEA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60021627,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1389,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "PEDRO GERARDO HERNANDEZ PEREZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019153,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1707,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RAUL FERNANDO ROJAS ORTEGA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023266,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1154,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RICARDO CISNEROS MONTERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60017394,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2080,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "RUBEN MONTIEL MENDOZA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60031103,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1810,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SARA MARIA GONZALEZ GALLON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024756,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1715,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "UZIEL ENRIQUE LICONA FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60023290,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1854,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "VICTOR ANTONIO CASTILLO ANGELES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026110,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}
		],
		"AZT_EQUIPOCOBERTURA" : "1020"
	},
	"ModEditoresSala" : {
		"ListaEditoresSala" : [{
				"EmpleadoLlavePrimaria" : 1402,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ADRIAN RIOS ZAMUDIO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60019304,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1971,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ALLAN CLUA ARMAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60028248,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1994,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS ANTONIO TOLEDO OROZCO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60028660,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1645,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "CARLOS ENRIQUE CISNEROS GUERRERO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022698,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2075,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANIEL BARRERA MORENO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030883,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 2038,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "DANIELA ALEJANDRA DEL RIO LOREDO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029790,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1333,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "EILEEN NISHINO AIRIN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60018464,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1788,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ENRIQUE ALEJANDRO ORTEGA NARANJO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024386,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5424,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GERARDO CONTRERAS QUIROZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60032513,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5427,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "GUSTAVO BLANCAS HERNANDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60030977,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 569,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "ISRAEL SERVIN MARTINEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60007916,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1819,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JORGE ALBERTO MARTINEZ CASTILLO",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024935,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1836,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE ALBERTO MORALES RIVAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026049,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1844,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE JAVIER CRISOSTOMO LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026065,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1952,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JOSE MANUEL PEREZ MARTINEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60027929,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 708,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "JUAN CARLOS SANTOS FLORES",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60010952,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 4833,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "KENIA XOCHITL LÓPEZ HERNÁNDEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60029889,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 813,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "LUIS ANGEL GONZALEZ LOPEZ",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60012194,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1615,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARCOS BARRON CONTRERAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60022353,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 5776,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MARIANA VERAGEN ACOSTA SANCHEZ ALDANA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60034403,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 57,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MIGUEL ANGEL GOMEZ AGUILAR",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60000711,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 733,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "MOISES FRANCISCO ARAUJO EQUIGUAS",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60011182,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1810,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "SARA MARIA GONZALEZ GALLON",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60024756,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1922,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "VICTOR MANUEL RIVERA MIRANDA",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60027435,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}, {
				"EmpleadoLlavePrimaria" : 1838,
				"EmpleadoTipo" : {
					"EmpleadoTipo" : 0,
					"DescripcionTipoEmpleado" : ""
				},
				"EmpleadoNombre" : "YANNICK CANEK ANDRADE MILLAN",
				"EmpleadoUsr" : "",
				"EmpleadoStatus" : "",
				"EmpleadoNumero" : 60026051,
				"EmpleadoChk" : 0,
				"EmpleadoChkVisible" : "Collapsed"
			}
		],
		"AZT_EQUIPOCOBERTURA" : "1020"
	},
	"ModProgramas" : {
		"ListaProgramas" : [{
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "A+",
				"CvePrograma" : 2455,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 33,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "ALERTA MEXICO",
				"CvePrograma" : 1935,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "ALTA DEFINICIÓN",
				"CvePrograma" : 452,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "CAFE DEPORTE",
				"CvePrograma" : 99,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "CAFE DEPORTE EXPRESS",
				"CvePrograma" : 119,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "CAIGA QUIEN CAIGA",
				"CvePrograma" : 179,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "CANAL 24 HRS",
				"CvePrograma" : 131,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "CORAZON GRUPERO",
				"CvePrograma" : 2548,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "DEPORTE CALIENTE",
				"CvePrograma" : 260,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "DEPORTV",
				"CvePrograma" : 23,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "DEPORTV AZ. AMERICA",
				"CvePrograma" : 102,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "EL CLUB DE EVA",
				"CvePrograma" : 2624,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "EL SILLON DE PENSAR",
				"CvePrograma" : 2486,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "ES TENDENCIA",
				"CvePrograma" : 2461,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "EXATLON",
				"CvePrograma" : 2556,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "EXATLON FUERA DE LUGAR",
				"CvePrograma" : 2579,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "GUIONES CONTINUOS",
				"CvePrograma" : 2540,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "HECHOS AM",
				"CvePrograma" : 4,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "HECHOS MERIDIANO",
				"CvePrograma" : 3,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "HECHOS NOCHE",
				"CvePrograma" : 1,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "HECHOS NOCHE (INTERNET)",
				"CvePrograma" : 1757,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "HECHOS SABADO ",
				"CvePrograma" : 5,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "HIT M3",
				"CvePrograma" : 172,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "INFORMATIVO 40 MAÑANA",
				"CvePrograma" : 1053,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "INFORMATIVO 40 NOCHE",
				"CvePrograma" : 54,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "INFORMATIVO 40 TARDE",
				"CvePrograma" : 53,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LA ACADEMIA 2019",
				"CvePrograma" : 2877,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LA HISTORIA DETRAS DEL MITO",
				"CvePrograma" : 191,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LOCALES",
				"CvePrograma" : 145,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LOS PROTA NOCHE",
				"CvePrograma" : 2125,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LOS PROTA TARDE",
				"CvePrograma" : 2123,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LOS PROTAGONISTAS",
				"CvePrograma" : 21,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "LOS 25+",
				"CvePrograma" : 584,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "MARCADOR 40",
				"CvePrograma" : 2103,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "MASTER CHEF",
				"CvePrograma" : 2165,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "MATRACA",
				"CvePrograma" : 48,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 6,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "MERCADO AZTECA",
				"CvePrograma" : 2639,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "NOTICIAS DE IDA Y VUELTA",
				"CvePrograma" : 2462,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "NOTICIERO FINANCIERO",
				"CvePrograma" : 2116,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "NOTICIERO JAIME GUERRERO",
				"CvePrograma" : 2117,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "NOTICIERO JUAN CARLOS BARAJAS",
				"CvePrograma" : 2118,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "NOTICIERO NACIONAL AZT. AMERICA",
				"CvePrograma" : 255,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "NOTICIERO NACIONAL EDICIÓN NOCTURNA",
				"CvePrograma" : 2037,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "PASION DEPORTIVA",
				"CvePrograma" : 2015,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 3,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "REVISTA VENTANEANDO",
				"CvePrograma" : 583,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "TODO UN SHOW",
				"CvePrograma" : 2673,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "VA QUE VA",
				"CvePrograma" : 160,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "VENGA LA ALEGRIA",
				"CvePrograma" : 50,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 2,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "VENTANEANDO",
				"CvePrograma" : 24,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "VIDAS AL LIMITE",
				"CvePrograma" : 243,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 1,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 7,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "VOTA MEXICO",
				"CvePrograma" : 2727,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 2,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}, {
				"EsFiaNoticias" : "",
				"IdIBOP" : 0,
				"EsFia" : "",
				"NombreIBOP" : "",
				"DiasTransmision" : null,
				"FechaFin" : "\\/Date(-62135575200000)\\/",
				"FechaInicio" : "\\/Date(-62135575200000)\\/",
				"Canal" : 0,
				"Abreviatura2" : "",
				"Abreviatura" : "",
				"HoraTransmision" : "",
				"IdCC" : "",
				"EstatusPrograma" : "",
				"NombrePrograma" : "WEST COAST",
				"CvePrograma" : 81,
				"EsAztecaAmerica" : 0,
				"EsDeporteContacto" : 0,
				"MediaGridTotal" : null,
				"MediaGridActual" : null,
				"MediaGridRuta" : null,
				"CveFabrica" : 4,
				"CarpetaiNews" : null,
				"oEmpresa" : {
					"CveEmpresa" : 1,
					"NombreEmpresa" : null,
					"Estatus" : null,
					"Abreviatura" : null,
					"BanderaIngestionAzteca" : null
				},
				"TipoPrograma" : "",
				"DiaGrabacion" : "",
				"DiaEdicion" : "",
				"Guion" : 0,
				"Tema" : 0,
				"Productor" : null,
				"Invitados" : 0
			}
		],
		"AZT_PROGRAMA" : "1024"
	},
	"ModSeccionAgencia" : {
		"ListaAgencias" : [{
				"CveAgencia" : 1,
				"NombreAgencia" : "APTN",
				"Estatus" : 0
			}, {
				"CveAgencia" : 3,
				"NombreAgencia" : "CNN",
				"Estatus" : 0
			}, {
				"CveAgencia" : 12,
				"NombreAgencia" : "REUTERS",
				"Estatus" : 0
			}, {
				"CveAgencia" : 15,
				"NombreAgencia" : "CBS",
				"Estatus" : 0
			}, {
				"CveAgencia" : 97,
				"NombreAgencia" : "ENEXS",
				"Estatus" : 0
			}, {
				"CveAgencia" : 100,
				"NombreAgencia" : "Alianza Informativa (AIL)",
				"Estatus" : 0
			}, {
				"CveAgencia" : 101,
				"NombreAgencia" : "Archivo",
				"Estatus" : 0
			}, {
				"CveAgencia" : 102,
				"NombreAgencia" : "VOA (Voz de America)",
				"Estatus" : 0
			}, {
				"CveAgencia" : 104,
				"NombreAgencia" : "CCTV",
				"Estatus" : 0
			}, {
				"CveAgencia" : 105,
				"NombreAgencia" : "TV AZTECA",
				"Estatus" : 0
			}, {
				"CveAgencia" : 106,
				"NombreAgencia" : "OTRO",
				"Estatus" : 0
			}
		],
		"AZT_AGENCIA" : "1019"
	},
	"ModClasificacion" : {
		"ListaValores" : [{
				"ClasificacionLlavPr" : 0,
				"ClasificacionDescripcion" : "SIN CLASIFICACION",
				"ClasificacionAbreviatura" : "NA",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "",
				"ClasificacionTratamiento" : "",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : null,
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 1,
				"ClasificacionDescripcion" : "Libertad Individual",
				"ClasificacionAbreviatura" : "LI",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "La libertad consiste en hacer lo que nos plazca,siempre que no dañemos a los demás ni impidamos su libertad.\\n\\nLa libertad ha demostrado ser esencial para la convivencia pacífica,la prosperidad económica, los descubrimientos científicos y la innovación tecnológica.\\n\\nSe vive en libertad cuando todos los ciudadanos son individuos autónomos, con derechos, obligaciones y responsabilidades.\\n\\nLa libertad individual implica las libertades de expresión, de creencia, de emprender, de elegir, de comerciar, de consumir, de tránsito, etc.\\n\\nLa libertad es la raíz de la vida ética y la madurez del ser humano.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017115000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 2,
				"ClasificacionDescripcion" : "Estado Fuerte",
				"ClasificacionAbreviatura" : "EF",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "Un Estado fuerte protege eficazmente la vida, la seguridad, la propiedad y los derechos básicos de las personas.\\nNo es un Estado omnipresente, que rija todas las actividadesciudadanas con leyes, reglamentos, vigilancia y burocracia.\\nNo limita las libertades, sino al contrario, las garantiza.\\nDebe ser fuerte para poder de enfrentar a la delincuenciay para aplicar a todos las mismas leyes.\\nLa función esencial del Estado debe ser la procuración de justicia.\\nUn Estado fuerte la entiende como su máxima prioridad. \\n\\nUn Estado delgado es más eficiente y más ágil que uno obeso.\\nObstaculiza menos las actividades de las personas y las empresas, lo que permite un mayor desarrollo personal y una mayor generación de riqueza.\\nEs una carga fiscal menor para toda la sociedad y disminuye las posibilidades de los políticos para corromperse.\\nSi el Estado engorda, la sociedad corre el riesgo de ponerse al servicio del Estado. Y debe ser al revés: el Estado debe estar al servicio de la sociedad, para brindar servicios públicos, procurar justicia y garantizar las normas mínimas de convivencia.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017115000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 3,
				"ClasificacionDescripcion" : "Estado Eficiente",
				"ClasificacionAbreviatura" : "ED",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "Un Estado delgado es más eficiente y más ágil que uno obeso.\\nObstaculiza menos las actividades de las personas y las empresas, lo que permite un mayor desarrollo personal y una mayor generación de riqueza.\\nEs una carga fiscal menor para toda la sociedad y disminuye las posibilidades de los políticos para corromperse.\\nSi el Estado engorda, la sociedad corre el riesgo de ponerse al servicio del Estado. Y debe ser al revés: el Estado debe estar al servicio de la sociedad, para brindar servicios públicos, procurar justicia y garantizar las normas mínimas de convivencia.\\n",
				"ClasificacionTratamiento" : " Las Libertades Individuales, La democracia liberal, Respeto al voto",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1536680567000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 4,
				"ClasificacionDescripcion" : "Poder Diseminado",
				"ClasificacionAbreviatura" : "PDA",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "Cuando el poder se concentra en pocas manos es mucho más peligroso que cuando se reparte entre muchas y se contrarresta por otros poderes.\\nQuienes han abusado del poder no comparten una misma ideología sino las circunstancias en las que pudieron abusar de su poder.\\nLa mejor manera que se ha encontrado de prevenir y limitar los abusos del poder ha sido la división de poderes y su diseminación entre muchos actores sociales.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017115000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 5,
				"ClasificacionDescripcion" : "Participación Ciudadana",
				"ClasificacionAbreviatura" : "PC",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "Participar es una decisión personal y un acto colectivo.\\nParticipar significa “tomar parte en algo”, “recibir una parte de algo”,\\n“compartir”, “ser socio”. Intervenir en un grupo.\\nTodos participamos social y políticamente.\\nSocialmente porque hacemos cosas con otras personas,\\nen el trabajo, en la calle, en casa, en internet.\\nPolíticamente porque incluso aquellos que no votan\\ny no se ocupan de los asuntos públicos\\nestán dando un silencioso voto de confianza (a veces sin saberlo)\\na quienes toman las decisiones políticas.\\nParticipar políticamente no es una opción. Todos lo hacemos.\\nPero hay que hacerlo mejor.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017115000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 6,
				"ClasificacionDescripcion" : "Economía de Mercado",
				"ClasificacionAbreviatura" : "EM",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "Es la única que produce prosperidad incluyente\\ny sostenible a largo plazo.\\nPuede hacerlo porque:\\n1) aprovecha los conocimientos,\\nlas energías y la creatividad de muchísimas personas,\\n2) usa con mayor eficiencia los recursos,\\n3) distribuye el poder y las riquezas,\\nentre un mayor número de personas, y\\n4) la competencia es el remedio contra la incompetencia.\\nUna economía de mercado o de libre mercado\\nes aquella en la que la mayoría de los bienes\\npueden ser comprados y vendidos\\na los precios determinados por la oferta y la demanda.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017115000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 7,
				"ClasificacionDescripcion" : "Igualdad de oportunidades y sistema de apoyo social",
				"ClasificacionAbreviatura" : "SAD",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "El crecimiento económico nunca se presenta de manera uniforme.\\nA algunos les va muy bien, a otros regular y a otros mal.\\nAquellos a los que les va bien tienen una obligación moral\\ncon los demás, y en especial con los más débiles:\\nniños, ancianos, enfermos, discapacitados, menesterosos.\\nUna parte de los impuestos se usa para ayudar a los más débiles.\\nPero se gasta mal y no es suficiente.\\nEn las sociedades más exitosas y más inteligentes\\nlas personas fuertes se imponen este reto:\\nluchar contra el sufrimiento evitable.\\nPara ellos cuentan con sistemas eficientes de ayuda.\\nSin fortaleza, no se pude ayudar a los débiles.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017202000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}, {
				"ClasificacionLlavPr" : 8,
				"ClasificacionDescripcion" : "Racionalidad",
				"ClasificacionAbreviatura" : "RD",
				"ClasificacionEstatus" : 1,
				"ClasificacionObjetivo" : "La racionalidad es el ejercicio de la razón.\\nNos permite perfeccionar nuestra idea del mundo\\ny tomar buenas decisiones,\\nque mejoren nuestra vida y la de los demás.\\nEn el ámbito de las discusiones públicas, es fundamental\\npara evitar los errores que los líderes podrían cometer.\\nUna cosa es tener el poder y otra tener la razón.\\nLo ideal es que ambas cosas coincidan,\\ny para ello es indispensable la racionalidad.\\nSe trata de un valor democrático por excelencia,\\npues la democracia es un gobierno por discusión.",
				"ClasificacionTratamiento" : " ",
				"ClasificacionQueSi" : "",
				"ClasificacionQueNo" : "",
				"ClasificacionFechaIni" : "\\/Date(1541017147000)\\/",
				"ClasificacionFechaFin" : null,
				"ClasificacionHashtag" : "",
				"ClasificacionAntiValor" : ""
			}
		],
		"AZT_AGENDALIBERALVALORES" : "0"
	},
	"ModEventos" : {
		"ListaEventoAgendaPublica" : [{
				"EventoLlavPr" : 18940,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Actividad A\\u0026C en el marco de la visita de RBS",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Jalisco",
				"EventoCiudad" : "Guadalajara",
				"EventoFecha" : "05/11/2019",
				"EventoHora" : "09:25 AM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18939,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Actividad de A\\u0026C en el marco de la visita de RBS",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Coahuila de Zaragoza",
				"EventoCiudad" : "Torreón",
				"EventoFecha" : "03/09/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18825,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Alamos Alliance",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Sonora",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "14/02/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover el libre comercio y políticas públicas de liberalización para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18828,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Alamos Alliance",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Sonora",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "16/02/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover el libre comercio y políticas públicas de liberalización para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18827,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Alamos Alliance",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Sonora",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "15/02/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover el libre comercio y políticas públicas de liberalización para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18804,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "21/01/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18809,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "08/04/2019",
				"EventoHora" : "12:27 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18808,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "25/03/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18812,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "06/05/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18807,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "11/03/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18806,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "25/02/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18805,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "11/02/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18810,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Círculo de Estudios Bastiat",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "22/04/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad a través del Círculo Bastiat buscan que los jóvenes universitarios comprendan mejor las ideas de distintos autores liberales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19027,
				"ClasificacionId" : 5,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Comida RBS y  Líderes Kybernus",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "25/10/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "Ricardo Salinas Pliego por medio de Kybernus , busca impulsar a las y los jóvenes  como tomadores de decisiones y elementos de cambio para generar prosperidad incluyente y libre en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18815,
				"ClasificacionId" : 5,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Comida RBS y  Líderes Kybernus",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "05/04/2019",
				"EventoHora" : "02:30 PM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "Ricardo Salinas Pliego por medio de Kybernus , busca impulsar a las y los jóvenes  como tomadores de decisiones y elementos de cambio para generar prosperidad incluyente y libre en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18820,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Conversatorio con Jeff Deist",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "30/01/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover la discusión sobre las ideas de la libertad entre jóvenes académicos para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18822,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Conversatorio con María Blanco",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "21/02/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover la discusión sobre las ideas de la libertad entre jóvenes académicos para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18823,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Conversatorio con Yaron Brook",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "26/02/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover la discusión sobre las ideas de la libertad entre jóvenes académicos para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18824,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Cumbre Sonora",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Sonora",
				"EventoCiudad" : "Hermosillo",
				"EventoFecha" : "13/02/2019",
				"EventoHora" : "10:00 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover el libre comercio y la integración internacional del México en el mundo para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18893,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Curso de Notas Periodísticas para Redes Sociales",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "24/02/2019",
				"EventoHora" : "01:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad busca darle las armas a jóvenes libertarios para promover el discurso liberal a través del mundo digital y redes sociales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18835,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Desayuno conferencia con México Evalúa, COMEXI y Caminos de la Libertad",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Miguel Hidalgo",
				"EventoFecha" : "13/02/2019",
				"EventoHora" : "08:00 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad busca dar a conocer las características del abatimiento de la pobreza en los últimos dos siglos.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18906,
				"ClasificacionId" : 5,
				"EventoOrganizacion" : "Fundación Azteca",
				"EventoNombre" : "Encuentro de Organizaciones Civiles y Donativo Hormiga ",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "31/10/2019",
				"EventoHora" : "10:00 AM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Benjamín Salinas Pliego, a través Fundación Azteca, ha tocado y mejorado la vida de millones de personas, vinculando los problemas más apremiantes y necesidades de la sociedad con organizaciones y personas dispuestas a comprometerse",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18819,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Encuentro Nacional de Enlaces Kybernus 2019",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "19/01/2019",
				"EventoHora" : "10:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con el desarrollo de las regiones  donde operan sus negocios y por lo tanto, busca promover entornos y programas para crear valor económico, ambiental y social y así generar progreso y prosperidad incluyente.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18905,
				"ClasificacionId" : 7,
				"EventoOrganizacion" : "Fundación Azteca",
				"EventoNombre" : "Entrega Bono Educativo Plantel Azteca",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Gustavo A. Madero",
				"EventoFecha" : "17/09/2019",
				"EventoHora" : "10:00 AM",
				"EventoProposito" : "Impulsar el interés por la ciencia y la tecnología para el cambio positivo de nuestro entorno  (Por ejemplo: Concurso de robótica)",
				"EventoMensaje" : "Ricardo Benjamín Salinas Pliego, a través de Plantel Azteca, apoya la transformación de la vida de los niños , sus familias a través de la educación para crear valor y generar prosperidad incluyente en nuestra  comunidad.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18937,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Función \\" El Rey del barrio \\ " - Cineteca Nacional",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Benito Juárez",
				"EventoFecha" : "23/02/2019",
				"EventoHora" : "08:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18938,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Función \\" El Rey del barrio \\ " - Cineteca Nacional",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Benito Juárez",
				"EventoFecha" : "24/02/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18842,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Función privada \\" El Rey del barrio \\ " - CINETECA NACIONAL ",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Benito Juárez",
				"EventoFecha" : "25/02/2019",
				"EventoHora" : "07:30 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura (en colaboración con la Cineteca Nacional ), buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18898,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Gira RBS- Expo Orgullo de mi país en Mérida ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Yucatán",
				"EventoCiudad" : "Mérida",
				"EventoFecha" : "26/03/2019",
				"EventoHora" : "09:00 AM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura, buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18930,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Inauguración de la exposición \\" Orgullo de mi país \\ " - Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : "León",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "06:05 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18931,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Inauguración de la exposición \\" Tarjetas de visita \\ " - Festival Liberalia ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : "León",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18929,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Inauguración exposición \\" Fotografía arqueológica \\ " (MAHG) - Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : "León",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18845,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Inauguración Festival Liberalia ",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : "León",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura, buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18927,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "La Celestina (Compañía Nacional de Teatro) - Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "07/06/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18926,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "La Celestina (Compañía Nacional de Teatro) - Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "06/06/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18928,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "La Celestina (Compañía Nacional de Teatro) - Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "08/06/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18895,
				"ClasificacionId" : 5,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "La Participación de los Jóvenes en el Marco de las Libertades",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "12/02/2019",
				"EventoHora" : "05:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Kybernus fomentan la participación de las y los jóvenes líderes en el marco de la libertades a partir del contexto de nuestro país con el objetivo de  crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18907,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Liber 4 (Julio- Septiembre)",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "07/07/2019",
				"EventoHora" : "01:31 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura, buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18909,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Liber 5 (Octubre- Diciembre)",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "07/10/2019",
				"EventoHora" : "02:03 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura, buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18816,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Liberty Con",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "23/02/2019",
				"EventoHora" : "09:00 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan difundir las ideas del liberalismo y dar un espacio a los jóvenes que buscan mejorar al país para crear valor y generar prosperidad incluyente en nuestro país",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18817,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Liberty Con",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "24/02/2019",
				"EventoHora" : "09:00 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan difundir las ideas del liberalismo y dar un espacio a los jóvenes que buscan mejorar al país para crear valor y generar prosperidad incluyente en nuestro país",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19749,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Tabasco",
				"EventoCiudad" : "Villahermosa",
				"EventoFecha" : "30/03/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19758,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Chihuahua",
				"EventoCiudad" : "Chihuahua",
				"EventoFecha" : "25/05/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19755,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Baja California Sur",
				"EventoCiudad" : "La Paz",
				"EventoFecha" : "18/05/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19753,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Aguascalientes",
				"EventoCiudad" : "Aguascalientes ",
				"EventoFecha" : "13/04/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19751,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Hidalgo",
				"EventoCiudad" : "Pachuca de Soto",
				"EventoFecha" : "06/04/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19756,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Nayarit",
				"EventoCiudad" : "Tepic",
				"EventoFecha" : "18/05/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19762,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Yucatán",
				"EventoCiudad" : "Mérida",
				"EventoFecha" : "22/06/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19757,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Oaxaca",
				"EventoCiudad" : "Oaxaca de Juárez",
				"EventoFecha" : "25/05/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19754,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Chiapas",
				"EventoCiudad" : "Tuxtla Gutiérrez",
				"EventoFecha" : "13/04/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19750,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : "León",
				"EventoFecha" : "30/03/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19752,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Nuevo León",
				"EventoCiudad" : "Monterrey",
				"EventoFecha" : "06/04/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19761,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guerrero",
				"EventoCiudad" : "Acapulco de Juárez",
				"EventoFecha" : "22/06/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19759,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Coahuila de Zaragoza",
				"EventoCiudad" : "Torreón",
				"EventoFecha" : "08/06/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19760,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Liderazgo Liberal:Evolución y desafíos",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Sinaloa",
				"EventoCiudad" : "Culiacán",
				"EventoFecha" : "08/06/2019",
				"EventoHora" : "08:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "RBS y Kybernus fortalecen la opinión crítica de las y los jóvenes por medio del conocimiento y cuestionamiento del contexto liberal que las y los rodea.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18834,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Master class con Ricardo López Murphy",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Cuajimalpa de Morelos",
				"EventoFecha" : "12/02/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan que jóvenes estudiantes conozcan las experiencias liberales en Latino América.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18818,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Media Training",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "26/01/2019",
				"EventoHora" : "09:00 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad, buscan formar a jóvenes liberales para comunicarse mejor antes los medios de comunicación masiva para generar valor y crear prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18923,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Ópera \\" Die Entführung aus dem Serail(Mozart) \\ " -Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "08:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas,  buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18925,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Ópera \\" Die Entführung aus dem Serail(Mozart) \\ " -Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "09/06/2019",
				"EventoHora" : "06:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas,  buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18924,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Ópera \\" Die Entführung aus dem Serail(Mozart) \\ " -Festival Liberalia",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "07/06/2019",
				"EventoHora" : "08:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas,  buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19119,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Paternalismo liberal: una mirada objetivista",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Coyoacán",
				"EventoFecha" : "26/02/2019",
				"EventoHora" : "12:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Difundir la filosofía objetivista y analizar la tendencia del paternalismo liberal.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18814,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Premiación del Concurso de Jóvenes y Socios Caminos de la Libertad",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "04/04/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover en los jóvenes y en los socios de Grupo Salinas una reflexión sobre las ideas de la libertad para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18813,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Premio al Mérito Emprendedor-Comida con RBS",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "13/09/2019",
				"EventoHora" : "03:00 PM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con los jóvenes emprendedores por lo que reconoce los liderazgos que fomentan la creatividad, la cultura del mérito y el aprendizaje por medio del fracaso.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18811,
				"ClasificacionId" : 5,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Premio al Valor Ciudadano-Comida con RBS",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "17/05/2019",
				"EventoHora" : "03:00 PM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "Ricardo Salinas P. está comprometido con el fortalecimiento del valor ciudadano por lo que reconoce a los liderazgos que fomentan la Libertad, el Estado de Derecho, la Cultura de la Legalidad, el Fortalecimiento de la Sociedad Civil y la Igualdad.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19118,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Presentación de  libros clásicos y cómics",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Cuauhtémoc",
				"EventoFecha" : "12/02/2019",
				"EventoHora" : "11:39 AM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "RBS y Caminos de la Libertad buscan promover la lectura sobre temas liberales y presentar los cómics con los que se explican el porqué las cosas dentro de la economía deben de fluir libremente, esto con el fin de crear valor y  generar prosperidad .",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18922,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Presentación micrositio de consulta de Revista PLURAL ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "15/02/2019",
				"EventoHora" : "10:30 AM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear v",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18900,
				"ClasificacionId" : 7,
				"EventoOrganizacion" : "Fundación Azteca",
				"EventoNombre" : "Presentación Robot en Fundación Azteca con Antonio Domínguez",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "11/02/2019",
				"EventoHora" : "10:30 AM",
				"EventoProposito" : "Impulsar el interés por la ciencia y la tecnología para el cambio positivo de nuestro entorno  (Por ejemplo: Concurso de robótica)",
				"EventoMensaje" : "Ricardo Benjamín Salinas Pliego, a través de la ciencia y la tecnología, apoya la transformación de la vida de los jóvenes mediante la Robótica para crear valor y generar prosperidad incluyente en nuestra  comunidad.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18821,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Primer Reunión ORC´s 2019- Kybernus",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "15/01/2019",
				"EventoHora" : "10:30 AM",
				"EventoProposito" : "Identificar, desarrollar e impulsar jóvenes líderes para lograr una nueva cultura del liderazgo público y social en México",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con el desarrollo de las regiones  donde operan sus negocios y por lo tanto, busca promover entornos y programas para crear valor económico, ambiental y social y así generar progreso y prosperidad incluyente.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18903,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Publicación de LIBER 3 (abril - junio)",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "07/04/2019",
				"EventoHora" : "01:21 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura, buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18897,
				"ClasificacionId" : 8,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Publicación LIBER 2 - Edición de invierno ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "07/01/2019",
				"EventoHora" : "03:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura, buscan fomentar y difundir la cultura y las artes a la sociedad en general, para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18920,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Recorrido Museo de el Carmen - expo ODMP",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Álvaro Obregón",
				"EventoFecha" : "06/02/2019",
				"EventoHora" : "03:30 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear v",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 0,
				"ClasificacionId" : 0,
				"EventoOrganizacion" : "Sin Evento",
				"EventoNombre" : "Sin Evento",
				"EventoAsisteRbs" : "no",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "Sin Evento",
				"EventoEstado" : "Sin Evento\\r\\nSin Evento",
				"EventoCiudad" : "Sin Evento",
				"EventoFecha" : "31/12/2050",
				"EventoHora" : "07:30 PM",
				"EventoProposito" : "Sin Evento",
				"EventoMensaje" : "Sin Evento",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18932,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Strange Fruit - Festival Liberalia ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18936,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Strange Fruit - Festival Liberalia ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "09/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18935,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Strange Fruit - Festival Liberalia ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "08/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18934,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Strange Fruit - Festival Liberalia ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "07/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18933,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Arte y Cultura Grupo Salinas",
				"EventoNombre" : "Strange Fruit - Festival Liberalia ",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : " Por Definir",
				"EventoFecha" : "06/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Difundir las ideas y el conocimiento para impulsar el cambio positivo de nuestra sociedad",
				"EventoMensaje" : "Ricardo Salinas Pliego y Arte  Cultura Grupo Salinas, buscan fomentar y difundir la cultura y las artes como parte fundamental de una buena calidad de vida  de la sociedad en general para crear valor y generar prosperidad incluyente nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 18892,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "Taller de Fotografía para Redes Sociales",
				"EventoAsisteRbs" : "No",
				"EventoConfirmaRbs" : "",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Tlalpan",
				"EventoFecha" : "23/02/2019",
				"EventoHora" : "11:00 AM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Caminos de la Libertad busca darle las armas a jóvenes libertarios para promover el discurso liberal a través del mundo digital y redes sociales para crear valor y generar prosperidad incluyente en nuestro país.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 17479,
				"ClasificacionId" : 6,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Visita de Ricardo Salinas al estado de Coahuila",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Coahuila de Zaragoza",
				"EventoCiudad" : "Torreón",
				"EventoFecha" : "03/09/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Otro (Fortalecer la relación con el gobierno del estado de Coahuila, dar a conocer las acciones sociales del Grupo en la región e impulsar relaciones con empresarios y sociedad.)",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con el desarrollo de las regiones  donde operan sus negocios y por lo tanto, busca promover entornos y programas para crear valor económico, ambiental y social y así generar progreso y prosperidad incluyente.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 17478,
				"ClasificacionId" : 6,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Visita de Ricardo Salinas al estado de Guanajuato",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Guanajuato",
				"EventoCiudad" : "León",
				"EventoFecha" : "05/06/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Otro (Fortalecer la relación con el gobierno del estado de Guanajuato, dar a conocer las acciones sociales del Grupo en la región e impulsar relaciones con empresarios y sociedad.)",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con el desarrollo de las regiones  donde operan sus negocios y por lo tanto, busca promover entornos y programas para crear valor económico, ambiental y social y así generar progreso y prosperidad incluyente.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 17480,
				"ClasificacionId" : 6,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Visita de Ricardo Salinas al estado de Jalisco",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Jalisco",
				"EventoCiudad" : "Guadalajara",
				"EventoFecha" : "05/11/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Otro (Fortalecer la relación con el gobierno del estado de Jalisco, dar a conocer las acciones sociales del Grupo en la región e impulsar relaciones con empresarios y sociedad.)",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con el desarrollo de las regiones  donde operan sus negocios y por lo tanto, busca promover entornos y programas para crear valor económico, ambiental y social y así generar progreso y prosperidad incluyente.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 17477,
				"ClasificacionId" : 6,
				"EventoOrganizacion" : "Kybernus",
				"EventoNombre" : "Visita de Ricardo Salinas al estado de Yucatán",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Yucatán",
				"EventoCiudad" : "Mérida",
				"EventoFecha" : "26/03/2019",
				"EventoHora" : "09:30 AM",
				"EventoProposito" : "Otro (Fortalecer la relación con el gobierno del estado de Yucatán, dar a conocer las acciones sociales del Grupo en la región e impulsar relaciones con empresarios y sociedad.)",
				"EventoMensaje" : "Ricardo Salinas Pliego está comprometido con el desarrollo de las regiones  donde operan sus negocios y por lo tanto, busca promover entornos y programas para crear valor económico, ambiental y social y así generar progreso y prosperidad incluyente.",
				"EventoEtiqueta" : null
			}, {
				"EventoLlavPr" : 19007,
				"ClasificacionId" : 1,
				"EventoOrganizacion" : "Caminos de la Libertad",
				"EventoNombre" : "22 Aniversario de La Entrevista con Sarmiento",
				"EventoAsisteRbs" : "Sí",
				"EventoConfirmaRbs" : "Asistencia corfirmada",
				"EventoPais" : "México",
				"EventoEstado" : "Ciudad de México",
				"EventoCiudad" : "Miguel Hidalgo",
				"EventoFecha" : "10/06/2019",
				"EventoHora" : "07:00 PM",
				"EventoProposito" : "Organizar campañas y actividades para que la gente conozca la importancia y la necesidad de la libertad",
				"EventoMensaje" : "RBS y Sergio Sarmiento festejan el vigésimo segundo aniversario del programa La entrevista con Sarmiento.",
				"EventoEtiqueta" : null
			}
		],
		"AZT_EVENTOSAGENDAPUBLICA" : "0"
	},
	"ModDeportes" : null
}
